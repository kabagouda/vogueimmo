// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:app/main.dart';

void main() {
  testWidgets('First test', (widgetTester) async {
  final topTextFieldFinder = find.byKey(Key('topTextField'));
   await widgetTester.ensureVisible(topTextFieldFinder);
    await widgetTester.enterText(topTextFieldFinder, 'Hello');  // Type 'Hello'
    await widgetTester.pump();

    await widgetTester.pumpWidget(MyApp());
    expect(find.byType(Text), findsNWidgets(6));
// // Build our app and trigger a frame.
// await widgetTester.pumpWidget(MyApp());

// // Verify that our counter starts at 0.
// expect(find.text('0'), findsOneWidget);
// expect(find.text('1'), findsNothing);

// // Tap the '+' icon and trigger a frame.
// await widgetTester.tap(find.byIcon(Icons.add));
// await widgetTester.pump();

// // Verify that our counter has incremented.
// expect(find.text('0'), findsNothing);
// expect(find.text('1'), findsOneWidget);
  });

}
