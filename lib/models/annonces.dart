class Annonces {
  bool? success;
  List<Data>? data;

  Annonces({this.success, this.data});

  Annonces.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int? id;
  String? type;
  String? pays;
  String? ville;
  String? quartier;
  String? mention;
  String? description;
  Null? nbreEtages;
  Null? etageLocation;
  Null? chambres;
  Null? typeCuisine;
  Null? typeToilette;
  Null? salleAManger;
  Null? bureau;
  Null? cave;
  Null? buanderie;
  Null? terrasse;
  Null? electriciteType;
  Null? eau;
  Null? nbreDeMenage;
  String? telephone1;
  Null? telephone2;
  int? montant;
  String? avance;
  String? image;
  String? frequencePaiement;
  String? caution;
  int? statut;
  String? slug;
  int? userOwnerId;
  String? createdAt;
  String? updatedAt;
  Null? deletedAt;

  Data(
      {this.id,
      this.type,
      this.pays,
      this.ville,
      this.quartier,
      this.mention,
      this.description,
      this.nbreEtages,
      this.etageLocation,
      this.chambres,
      this.typeCuisine,
      this.typeToilette,
      this.salleAManger,
      this.bureau,
      this.cave,
      this.buanderie,
      this.terrasse,
      this.electriciteType,
      this.eau,
      this.nbreDeMenage,
      this.telephone1,
      this.telephone2,
      this.montant,
      this.avance,
      this.image,
      this.frequencePaiement,
      this.caution,
      this.statut,
      this.slug,
      this.userOwnerId,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    pays = json['pays'];
    ville = json['ville'];
    quartier = json['quartier'];
    mention = json['mention'];
    description = json['description'];
    nbreEtages = json['nbre_etages'];
    etageLocation = json['etage_location'];
    chambres = json['chambres'];
    typeCuisine = json['type_cuisine'];
    typeToilette = json['type_toilette'];
    salleAManger = json['salle_a_manger'];
    bureau = json['bureau'];
    cave = json['cave'];
    buanderie = json['buanderie'];
    terrasse = json['terrasse'];
    electriciteType = json['electricite_type'];
    eau = json['eau'];
    nbreDeMenage = json['nbre_de_menage'];
    telephone1 = json['telephone1'];
    telephone2 = json['telephone2'];
    montant = json['montant'];
    avance = json['avance'];
    image = json['image'];
    frequencePaiement = json['frequence_paiement'];
    caution = json['caution'];
    statut = json['statut'];
    slug = json['slug'];
    userOwnerId = json['user_owner_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['pays'] = this.pays;
    data['ville'] = this.ville;
    data['quartier'] = this.quartier;
    data['mention'] = this.mention;
    data['description'] = this.description;
    data['nbre_etages'] = this.nbreEtages;
    data['etage_location'] = this.etageLocation;
    data['chambres'] = this.chambres;
    data['type_cuisine'] = this.typeCuisine;
    data['type_toilette'] = this.typeToilette;
    data['salle_a_manger'] = this.salleAManger;
    data['bureau'] = this.bureau;
    data['cave'] = this.cave;
    data['buanderie'] = this.buanderie;
    data['terrasse'] = this.terrasse;
    data['electricite_type'] = this.electriciteType;
    data['eau'] = this.eau;
    data['nbre_de_menage'] = this.nbreDeMenage;
    data['telephone1'] = this.telephone1;
    data['telephone2'] = this.telephone2;
    data['montant'] = this.montant;
    data['avance'] = this.avance;
    data['image'] = this.image;
    data['frequence_paiement'] = this.frequencePaiement;
    data['caution'] = this.caution;
    data['statut'] = this.statut;
    data['slug'] = this.slug;
    data['user_owner_id'] = this.userOwnerId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}
