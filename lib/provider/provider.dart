import 'package:flutter/material.dart';

class LoginProvider with ChangeNotifier {
  bool _isLogged = false;
  bool get isLogged => _isLogged;
  // void increment() {
  //   _count++;
  //   notifyListeners(); //important
  // }
  void changeLoggedStatus() {
    _isLogged = !_isLogged;
    notifyListeners();
  }

  //----------------LoginData
  Map _loginData = {};
  Map get loginData => _loginData;
  // assign new value to _loginData
  set loginData(Map value) {
    _loginData = value;
    notifyListeners();
  }
}
