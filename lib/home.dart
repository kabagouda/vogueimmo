import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'provider/provider.dart';
import 'screens/home_screens/acceuil.dart';
import 'screens/home_screens/annonces.dart';
import 'screens/home_screens/locataires.dart';
import 'screens/home_screens/paiements.dart';
import 'screens/home_screens/profil.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _selectedIndex = 0;
  static List<Widget> pages = <Widget>[
    Container(
      child: Acceuil(),
      color: Color(0xFFE0E0E0),
    ),
    Container(
      child: Annonces(),
      color: Color(0xFFE0E0E0),
    ),
    Container(
      child: Locataires(),
      color: Color(0xFFE0E0E0),
    ),
    Container(
      child: Paiements(),
      color: Color(0xFFE0E0E0),
    ),
    Container(
      child: Profil(),
      color: Color(0xFFE0E0E0),
    ),
  ];
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: _buildDrawer(),
      body: pages[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.purple,
          unselectedItemColor: Colors.black,
          backgroundColor: Colors.black,
          selectedLabelStyle: TextStyle(
              fontSize: 12, fontWeight: FontWeight.bold, color: Colors.purple),
          unselectedLabelStyle: TextStyle(
              fontSize: 12, fontWeight: FontWeight.bold, color: Colors.black),
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
          items: [
            // Acceuil
            BottomNavigationBarItem(
                icon: Image(
                  image: AssetImage('assets/icons/acceuil.png'),
                ),
                activeIcon: Image(
                  image: AssetImage('assets/icons/acceuil.png'),
                ),
                label: 'Acceuil'),
            // Annonces
            BottomNavigationBarItem(
                icon: Image(
                  image: AssetImage('assets/icons/annonces.png'),
                ),
                activeIcon: Image(
                  image: AssetImage('assets/icons/annonces.png'),
                ),
                label: 'Annonces'),
            // Locataires
            BottomNavigationBarItem(
                icon: Image(
                  image: AssetImage('assets/icons/locataires.png'),
                ),
                activeIcon: Image(
                  image: AssetImage('assets/icons/locataires.png'),
                ),
                label: 'Locataires'),
            // Paiement
            BottomNavigationBarItem(
                icon: Image(
                  image: AssetImage('assets/icons/paiements.png'),
                ),
                activeIcon: Image(
                  image: AssetImage('assets/icons/paiements.png'),
                ),
                label: 'Paiements'),
            // Profil
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.person,
                  color: Colors.black,
                ),
                activeIcon: Icon(
                  Icons.person,
                  color: Colors.purple,
                ),
                label: 'Profil'),
          ]),
    );
  }

  Widget _buildDrawer() {
    Map _userData =
        Provider.of<LoginProvider>(context, listen: false).loginData;
    return Drawer(
      child: context.watch<LoginProvider>().isLogged == false
          ? Center(child: Text('Vous n\'êtes pas connecté'))
          : ListView(
              children: [
                DrawerHeader(
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    child: Icon(
                      Icons.add_a_photo,
                      // size: MediaQuery.of(context).size.height * 0.1,
                      color: Colors.black,
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.purple,
                  ),
                ),
                ListTile(
                  title: Text(
                    'Nom :     ${_userData['lastname']}',
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Home()),
                    );
                  },
                ),
                Divider(),
                ListTile(
                  title: Text(
                    'Prénom :    ${_userData['name']}',
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Home()),
                    );
                  },
                ),
                Divider(),
                ListTile(
                  title: Text(
                    'Email:    ${_userData['email']}',
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Home()),
                    );
                  },
                ),
                Divider(),
                ListTile(
                  title: Text(
                    'Telephone:     ${_userData['phone']}',
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Home()),
                    );
                  },
                ),
                Divider()
              ],
            ),
    );
  }
}
