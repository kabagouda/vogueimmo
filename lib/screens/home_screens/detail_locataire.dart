import 'package:flutter/material.dart';

class DetailLocataire extends StatefulWidget {
  const DetailLocataire({ Key? key }) : super(key: key);
 
  @override
  _DetailLocataireState createState() => _DetailLocataireState();
}

class _DetailLocataireState extends State<DetailLocataire> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
       body: SafeArea(
         child: Column(children: [
           Row(
             children: [
                Padding(
                padding: EdgeInsets.only(left: width * 0.05),
                child: IconButton(
                  icon: Icon(Icons.arrow_back , size: 28),
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                ),
              ),
              // Text : Paiements
              Padding(
                padding: EdgeInsets.only(right: width * 0.05),
                child: Text(
                  'Detail Locataire',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
              ),
                         ],
           ),

           // CircleAvatar Image
            Padding(
              padding: EdgeInsets.only(top: width * 0.05),
              child: CircleAvatar(
                radius: width * 0.1,
                backgroundImage: AssetImage('assets/icons/profil.png'),
              ),
            ),
          // Informations Locataire
          Text(
            'Informations Locataire',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: width * 0.05),
            decoration: BoxDecoration(
              color: Color(0xFFE0E0E0),
              borderRadius: BorderRadius.circular(10),
            
            ),
            child: Column(
              children: [
                Text('Nom : Doe'),
                Text('Prenom : John'),
                Text('Genre : Homme'),
                Text('Date de naissance : 12/12/2000'),
                Text('Email : johndoe@gmail.com'),
                Text('Telephone : 0661234567'),
              ],
            ),
          )
         ],),
       )
    );
  }
}