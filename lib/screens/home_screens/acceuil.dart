import 'dart:math';

import 'package:app/functions/getAnncones.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class Acceuil extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getAnnconces(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          print('waiting');   // for testing
          return Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.purple,
            ),
          );
        } else if (snapshot.hasError) {
          print('error...........................'); // for testing
          return SafeArea(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.wifi_off, color: Colors.purple, size: 100),
                  SizedBox(height: 20),
                  Text('Erreur de connexion' ),
                ],
              ),),
          );
        } else if (snapshot.connectionState == ConnectionState.done) {
          Map data = snapshot.data as Map;
          return AcceuilHead(data);
        } else{
          return Center(
            child: Text('Désolé , une erreur s\'est produite , réessayer'),);
        }
      },
    );
  }
}

class AcceuilHead extends StatefulWidget {
  final Map data;
  AcceuilHead(this.data, {Key? key}) : super(key: key);

  @override
  State<AcceuilHead> createState() => _AcceuilHeadState();
}

class _AcceuilHeadState extends State<AcceuilHead> {
  @override
  Widget build(BuildContext context) {
    List listOfData = widget.data['data'];
    final width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return ListView(
      children: <Widget>[
        // Row widget with children Drawer icon and Notification icon
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: width * 0.05),
              child: IconButton(
                icon: Icon(Icons.menu, size: 28),
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: width * 0.05),
              child: IconButton(
                icon: Icon(Icons.notifications, size: 28),
                onPressed: () {},
              ),
            ),
          ],
        ),
        // Search bar with search icon at the left
        Padding(
          padding: EdgeInsets.only(
              top: height * 0.01, left: width * 0.05, right: width * 0.05),
          child: Container(
            height: height * 0.08,
            width: width * 0.9,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: Row(
              children: <Widget>[
                // Textfield at the right and Icon inside a purple container at the left
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: width * 0.05),
                    child: TextField(
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Rechercher',
                        hintStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: width * 0.05),
                  child: Container(
                    height: height * 0.05,
                    width: height * 0.05,
                    decoration: BoxDecoration(
                      color: Colors.purple,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Icon(
                      Icons.search,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        // Text widget with title
        Padding(
          padding: EdgeInsets.only(
            top: height * 0.01,
            left: width * 0.05,
          ),
          child: Text(
            'Explorer',
            style: TextStyle(
              fontSize: 27,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        // Little text : les plus vues
        Padding(
          padding: EdgeInsets.only(top: height * 0.01, left: width * 0.05),
          child: Text(
            'Les plus vues',
            style: TextStyle(
                fontSize: 15, color: Colors.grey, fontWeight: FontWeight.bold),
          ),
        ),
        // a container containing 4 images with a carousel slider widget
        Container(
          height: height * 0.2,
          child: CarouselSlider(
            options: CarouselOptions(
              height: height * 0.3,
              autoPlay: true,
              autoPlayInterval: Duration(seconds: 5),
              autoPlayAnimationDuration: Duration(milliseconds: 1000),
              autoPlayCurve: Curves.fastOutSlowIn,
              enlargeCenterPage: true,
              scrollDirection: Axis.horizontal,
            ),
            items: listOfData.map((i) {
              return Builder(
                builder: (BuildContext context) {
                  return
                      // a stack of a container and a column of two texts at the bottom right
                      Stack(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.symmetric(horizontal: 5.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          image: DecorationImage(
                            image: NetworkImage(
                                'https://vogueimmo.com${i['image']}'),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        left: 0,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                  left: width * 0.05, bottom: height * 0.01),
                              child: Text(
                                '${i['type']} à ${i['ville']}',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: width * 0.05, bottom: height * 0.01),
                              child: Container(
                                margin: EdgeInsets.symmetric(horizontal: 1),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Text(
                                  '${i['montant']}/m',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                },
              );
            }).toList(),
          ),
        ),
        // Little text : Catégories
        Padding(
          padding: EdgeInsets.only(
              top: height * 0.01, bottom: height * 0.01, left: width * 0.05),
          child: Text(
            'Catégories',
            style: TextStyle(
                fontSize: 15, color: Colors.grey, fontWeight: FontWeight.bold),
          ),
        ),
        // Two row widgets with two rows of purple container containing home icon and a little bit title
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding:
                    EdgeInsets.only(left: width * 0.05, right: width * 0.02),
                child: Container(
                  height: height * 0.08,
                  width: width * 0.4,
                  decoration: BoxDecoration(
                    color: Colors.purple,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Image(
                        image: AssetImage('assets/icons/house1.png'),
                        height: height * 0.04,
                      ),
                      Text(
                        'Maisons',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding:
                    EdgeInsets.only(right: width * 0.05, left: width * 0.02),
                child: Container(
                  height: height * 0.08,
                  width: width * 0.4,
                  decoration: BoxDecoration(
                    color: Colors.purple,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Image(
                          image: AssetImage('assets/icons/appartment1.png'),
                          height: height * 0.05,
                        ),
                        Text(
                          'Appartements',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                      ]),
                ),
              ),
            ]),
        //  Proche de chez vous
        Padding(
          padding: EdgeInsets.only(
              top: height * 0.01, bottom: height * 0.01, left: width * 0.05),
          child: Text(
            'Proche de chez vous',
            style: TextStyle(
                fontSize: 15, color: Colors.grey, fontWeight: FontWeight.bold),
          ),
        ),

        // a listview builder inside this listview with a container with a stack of an image and a column of two texts at the bottom right
        ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemCount: 4,
          itemBuilder: (context, index) {
            //TODO : here to add random data to the list
            var random = Random();
            List extractRandomListIndex  = List.generate(4, (i) {
              return random.nextInt(listOfData.length);
            });
            List extractRandomList  = List.generate(4, (i) {
              return listOfData[extractRandomListIndex[i]];
            });

            print(extractRandomListIndex);
            print(extractRandomList);
            return Padding(
              padding: EdgeInsets.all(width * 0.03),
              child: Container(
                height: height * 0.25,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  image: DecorationImage(
                    image: NetworkImage('https://vogueimmo.com${extractRandomList[index]['image']}'),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      bottom: 0,
                      left: 0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(
                                left: width * 0.05, bottom: height * 0.01),
                            child: Text(
                             '${extractRandomList[index]['type']} à ${extractRandomList[index]['ville']}',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: width * 0.05, bottom: height * 0.01),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Text(
                                '${extractRandomList[index]['montant']}/m',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
