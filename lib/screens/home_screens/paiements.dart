import 'package:flutter/material.dart';

class Paiements extends StatefulWidget {
  const Paiements({Key? key}) : super(key: key);

  @override
  State<Paiements> createState() => _PaiementsState();
}

class _PaiementsState extends State<Paiements> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return
        // Column inside Listview
        ListView(
      children: <Widget>[
        // Row widget with children Drawer icon and Notification icon
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: width * 0.05),
              child: IconButton(
                icon: Icon(Icons.menu, size: 28),
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
              ),
            ),
            // Text : Paiements
            Padding(
              padding: EdgeInsets.only(right: width * 0.05),
              child: Text(
                'Paiements',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: width * 0.05),
              child: IconButton(
                icon: Icon(Icons.notifications, size: 28),
                onPressed: () {},
              ),
            ),
          ],
        ),
        // Search bar with search icon at the left
        // Two row of a date and a dropdownButton to let user select a begining date and an end date by using date picker package

        Padding(
          padding: EdgeInsets.only(
              top: height * 0.05, left: width * 0.05, right: width * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Du',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                'Au',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: height * 0.01, left: width * 0.03),
              child: Container(
                height: height * 0.07,
                width: width * 0.47,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Row(
                  children: <Widget>[
                    // Textfield at the right and Icon inside a purple container at the left
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left: width * 0.05),
                        child: TextField(
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: '01/01/2022',
                            hintStyle: TextStyle(
                              color: Colors.grey,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: width * 0.05),
                      child: Container(
                        height: height * 0.05,
                        width: height * 0.05,
                        decoration: BoxDecoration(
                          color: Colors.purple,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        // Here
                        child: _buildDatePickerButton(
                          child: Icon(
                            Icons.calendar_today,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            // Container with two date pickers
            //
            // SizedBox(
            //   width: width * 0.01,
            // ),
            Padding(
              padding: EdgeInsets.only(top: height * 0.01, right: width * 0.03),
              child: Container(
                height: height * 0.07,
                width: width * 0.47,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Row(
                  children: <Widget>[
                    // Textfield at the right and Icon inside a purple container at the left
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left: width * 0.05),
                        child: TextField(
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: '01/02/2022',
                            hintStyle: TextStyle(
                              color: Colors.grey,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: width * 0.05),
                      child: Container(
                        height: height * 0.05,
                        width: height * 0.05,
                        decoration: BoxDecoration(
                          color: Colors.purple,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: _buildDatePickerButton(
                          child: Icon(
                            Icons.calendar_today,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),

        // // Container with two date pickers

        //------------------------------------------------------

        Padding(
          padding: EdgeInsets.only(top: height * 0.02),
          child: ListView.builder(
            itemCount: 10,
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemBuilder: (context, index) {
              return Padding(
                padding: EdgeInsets.only(
                    top: height * 0.02,
                    left: width * 0.05,
                    right: width * 0.05),
                child: Container(
                  height: height * 0.12,
                  width: width * 0.9,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Row(children: <Widget>[
                    // Container of profil image
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.05),
                      child: Container(
                        height: height * 0.07,
                        width: height * 0.07,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          image: DecorationImage(
                            image: AssetImage('assets/icons/salary.png'),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    // Container of name and location info
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.05),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'John Doe',
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Colors.purple,
                            ),
                          ),
                          Text(
                            '12/10/2021',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: width * 0.07,
                    ),

                    //Container of money
                    Container(
                      height: height * 0.05,
                      decoration: BoxDecoration(
                        color: Color(0xFF298F45),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: width * 0.01, right: width * 0.01),
                        child: Center(
                          child: Text(
                            '100.000 XOF',
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    )
                  ]),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _buildDatePickerButton({required Widget child}) {
    return TextButton(
        onPressed: () {
          // DatePicker.showDatePicker(context,
          //     showTitleActions: true,
          //     minTime: DateTime(2022, 1, 1),
          //     maxTime: DateTime(2023, 6, 7), onChanged: (date) {
          //   print('change $date');
          // }, onConfirm: (date) {
          //   print('confirm $date');
          // }, currentTime: DateTime.now(), locale: LocaleType.fr);
        },
        child: child);
  }
}
