import 'package:flutter/material.dart';

import 'detail_locataire.dart';

class Locataires extends StatefulWidget {
  const Locataires({Key? key}) : super(key: key);

  @override
  State<Locataires> createState() => _LocatairesState();
}

class _LocatairesState extends State<Locataires> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return
        // Column inside Listview
        ListView(
      children: <Widget>[
        // Row widget with children Drawer icon and Notification icon
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: width * 0.05),
              child: IconButton(
                icon: Icon(Icons.menu, size: 28),
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
              ),
            ),
            // Text : Locataires
            Padding(
              padding: EdgeInsets.only(right: width * 0.05),
              child: Text(
                'Locataires',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: width * 0.05),
              child: IconButton(
                icon: Icon(Icons.notifications, size: 28),
                onPressed: () {},
              ),
            ),
          ],
        ),
        // Search bar with search icon at the left
        Padding(
          padding: EdgeInsets.only(
              top: height * 0.05, left: width * 0.05, right: width * 0.05),
          child: Container(
            height: height * 0.08,
            width: width * 0.9,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: Row(
              children: <Widget>[
                // Textfield at the right and Icon inside a purple container at the left
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: width * 0.05),
                    child: TextField(
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Rechercher',
                        hintStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: width * 0.05),
                  child: Container(
                    height: height * 0.05,
                    width: height * 0.05,
                    decoration: BoxDecoration(
                      color: Colors.purple,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Icon(
                      Icons.search,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        // ListView Builder  with Container of profil image , name: John Doe and location info : Paris
        Padding(
          padding: EdgeInsets.only(top: height * 0.02),
          child: ListView.builder(
            itemCount: 10,
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemBuilder: (context, index) {
              return Padding(
                padding: EdgeInsets.only(
                    top: height * 0.02, left: width * 0.05, right: width * 0.05),
                child: Container(
                  height: height * 0.15,
                  width: width * 0.9,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Row(children: <Widget>[
                    // Container of profil image
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.05),
                      child: Container(
                        height: height * 0.1,
                        width: height * 0.1,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          image: DecorationImage(
                            image: AssetImage('assets/images/profil.jpg'),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    // Container of name and location info
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.05),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          TextButton(
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(
                                builder: (context) {
                                  return DetailLocataire();
                                },
                              ));
                            },
                            child: Text(
                              'John Doe',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                 color: Colors.purple
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Immeuble : B5',
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.grey,
                                ),
                              ),
                              SizedBox(
                                width: width * 0.05,
                              ),
                              Text(
                                'Location 1',
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.grey,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
