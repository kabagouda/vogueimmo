// login screen
import 'dart:io';

import 'package:app/functions/login.dart';
import 'package:app/provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../home.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  String? _email;
  String? _password;
  bool _isLogged = false;
  bool _showPassword = false;
  bool _showErrorMessage = false;
  bool _isObscureText = true;

  // internet connexion
  bool _isConnected = false;

  @override
  Widget build(BuildContext context) {
    // final width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
                top: height * 0.08, left: width * 0.05, right: width * 0.05),
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Image(
                      image: AssetImage('assets/favicon.png'),
                      width: height * 0.23,
                      // width: height * 0.9,
                    ),
                    height: MediaQuery.of(context).size.height / 4,
                  ),
                  Container(
                    child: Text(
                      'Connexion',
                      // style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                      style: GoogleFonts.poppins(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    padding: EdgeInsets.only(
                      bottom: height * 0.01,
                    ),
                  ),

                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Container(
                          child: TextFormField(
                            controller: _emailController,
                            decoration: InputDecoration(
                                hintText: 'Email',
                                hintStyle: TextStyle(fontSize: 15),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10))),
                            validator: (value) {
                              if (value?.isEmpty == true) {
                                return 'L\'email ne peut pas être vide';
                              } else if (_showErrorMessage == true) {
                                return 'Identifiant ou mot de passe incorrect';
                              }
                            },
                            onSaved: (newValue) => _email = newValue?.trim(),
                          ),
                          padding: EdgeInsets.only(top: height * 0.04),
                        ),

                        //----------------Password
                        Container(
                          child: TextFormField(
                            controller: _passwordController,
                            decoration: InputDecoration(
                                hintText: 'Mot de passe',
                                hintStyle: TextStyle(fontSize: 15),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10))),
                            obscureText: false,
                            validator: (value) {
                              if (value?.isEmpty == true) {
                                return 'Le mot de passe ne peut pas être vide';
                              } else if (_showErrorMessage == true) {
                                return 'Identifiant ou mot de passe incorrect';
                              }
                            },
                            onSaved: (newValue) => _password = newValue?.trim(),
                          ),
                          padding: EdgeInsets.only(top: height * 0.02),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: ElevatedButton(
                      onPressed: () async {
                        // Here
                        setState(() {
                          _showErrorMessage = false;
                        });
                        if (_formKey.currentState?.validate() == true) {
                          _formKey.currentState?.save();
                          print(_email);
                          print(_password);
                          try {
                            Map _resultOfLogin = await login(_email!, _password!);
                            //For test :
                            print(_resultOfLogin);
                            if (_resultOfLogin['success'] == true) {
                              // Change logged status
                              Provider.of<LoginProvider>(context, listen: false)
                                  .changeLoggedStatus();
                              // Assign new value to _loginData
                              Provider.of<LoginProvider>(context, listen: false)
                                  .loginData = _resultOfLogin;
                              // Navigate to home screen
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(
                                builder: (context) {
                                  return Home();
                                },
                              ));
                            } else {
                              setState(() {
                                _showErrorMessage = true;
                                _formKey.currentState?.validate();
                              });
                            }
                          } on SocketException {
                            // If internet is not connected , show a  snackbar
                            ScaffoldMessenger.of(context)
                                .showMaterialBanner(_buildMaterialBanner2());
                            Future.delayed(Duration(seconds: 4), () {
                              ScaffoldMessenger.of(context)
                                  .hideCurrentMaterialBanner(); //to hide
                            });

                            // For test

                          } catch (e) {
                            print(e);
                          }
                        }
                      },
                      child: Text(
                        'Se Connecter',
                        style: TextStyle(color: Colors.white),
                      ),
                      style: ElevatedButton.styleFrom(
                        primary: Colors.purple,
                        // borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                    padding: EdgeInsets.only(top: height * 0.02),
                  ),
                  Container(
                    child: Text(
                      'Mot de passe oublié ?',
                      style: TextStyle(fontSize: 15, color: Colors.blue),
                    ),
                    padding: EdgeInsets.only(top: height * 0.02),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Text(
                          'Vous n\'avez pas encore de compte  ?',
                          style: TextStyle(fontSize: 15, color: Colors.black),
                        ),
                        padding: EdgeInsets.only(top: height * 0.04),
                      ),
                      Container(
                        child: TextButton(
                          onPressed: () {
                            ScaffoldMessenger.of(context)
                                .showMaterialBanner(_buildMaterialBanner());
                            Future.delayed(Duration(seconds: 4), () {
                              ScaffoldMessenger.of(context)
                                  .hideCurrentMaterialBanner(); //to hide
                            });
                          },
                          child: Text(
                            'S\'inscrire',
                            style: TextStyle(
                                color: Colors.purple,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        padding: EdgeInsets.only(top: height * 0.04),
                      ),
                    ],
                  ),

                  //Text button to skip the login
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      child: TextButton(
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(
                            builder: (context) {
                              return Home();
                            },
                          ));
                        },
                        child: Text(
                          'Passer cette etape',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.blue,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      padding: EdgeInsets.only(top: height * 0.02),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  MaterialBanner _buildMaterialBanner() {
    return MaterialBanner(
      backgroundColor: Colors.purple,
      content: Text(
        'Pour s\'inscrire, rendez-vous sur vogueimmo.com',
        style: TextStyle(color: Colors.white),
      ),
      actions: [
        TextButton(
          onPressed: () =>
              ScaffoldMessenger.of(context).hideCurrentMaterialBanner(),
          child: Text(
            'OK',
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      ],
    );
  }

  MaterialBanner _buildMaterialBanner2() {
    return MaterialBanner(
      backgroundColor: Colors.purple,
      content: Text(
        'Veuillez vérifier votre connexion internet',
        style: TextStyle(color: Colors.white),
      ),
      actions: [
        TextButton(
          onPressed: () =>
              ScaffoldMessenger.of(context).hideCurrentMaterialBanner(),
          child: Text(
            'OK',
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      ],
    );
  }
}
