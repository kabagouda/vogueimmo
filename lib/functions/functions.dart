
// import 'package:flutter/material.dart';

// class Name_Provider with ChangeNotifier {    //notifyListeners() in each method
  
//   Map _loginData = {};
//   Map get loginData =>_loginData;
//   // assign new value to _loginData
//   set loginData(Map value) {
//     _loginData = value;
//     notifyListeners();
//   }
//   // int _count = 0;
//   // int get count => _count;

//   // void new() {
//   //   _count++;
//   //   notifyListeners();
//   // }
//   // ----Make yours
// }
 
 
// //     ##---> Usage: 
// // Provider.of<Name_Provider>(context , listen: false).increment()
 
// //  or--- 
//  // context.watch<Name_Provider>().count
//  // context.read<Name_Provider>().increment()